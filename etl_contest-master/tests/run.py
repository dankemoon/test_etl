from fixtures import databases_config, mysql_src_credentials, mysql_dst_credentials, mysql_source, mysql_destination
from test_etl_contest import test_containers_assets_is_ready, test_data_transfer
import sys

sys.path.append(r'etl_contest-master\etl')
from etl import etl, etl_2


conf = databases_config()
src_creds = mysql_src_credentials(conf)
dst_creds = mysql_dst_credentials(conf)


def init_db(src_creds, dst_creds):
    mysql_source(src_creds)
    mysql_destination(dst_creds)


init_db(src_creds, dst_creds)
test_containers_assets_is_ready(src_creds, dst_creds)

etl_2(src_creds, dst_creds)

test_data_transfer(src_creds, dst_creds)
