from datetime import datetime
import pymysql
import pandas as pd
from sqlalchemy import create_engine


def etl(src_creds: dict, dst_creds: dict):

    def get_conn(creds: dict):
        conn_str = f"mysql://{creds['user']}:{creds['password']}@{creds['host']}:{creds['port']}/{creds['database']}"
        engine = create_engine(conn_str)
        conn = engine.connect()
        return conn
    
    def get_cutoff(dst_creds, field, dst_table_name):
        sql = f"select max({field}) from {dst_table_name}"
        with pymysql.connect(**dst_creds, autocommit=True) as conn, conn.cursor() as cur:
            cur.execute(sql)
            cutoff = cur.fetchone()
        if cutoff[0]:
            return str(cutoff[0]) # class 'datetime.datetime'

    def get_src_df(src_conn, cutoff):
        sql = f"""
            select 
                t.*
                ,o.name as name_oper
            from sandbox.transactions t
            left join sandbox.operation_types o
                on t.idoper = o.id
        """
        if cutoff:
            sql+=f"where t.dt > '{cutoff}' and t.dt <= '{cutoff}' + interval 1 hour"
        else:
            sql+="order by t.dt limit 2" # стартовая проливка
        print(sql)
        df = pd.read_sql(sql, src_conn)
        return df

    src_conn = get_conn(src_creds)
    dst_conn = get_conn(dst_creds)

    cnt = 0
    cutoff = get_cutoff(dst_creds, 'dt', 'sandbox.transactions_denormalized')
    df = get_src_df(src_conn, cutoff)

    while not df.empty:
        print(cnt)
        df.to_sql(name='transactions_denormalized', con=dst_conn, if_exists='append', index=False)
        val = get_cutoff(dst_creds, 'dt', 'sandbox.transactions_denormalized')
        if val and val != cutoff:
            cutoff = val
        else:
            print('no loaded data since the last iteration')
            break
        df = get_src_df(src_conn, cutoff)
        cnt+=1


def etl_2(src_creds: dict, dst_creds: dict):

    def get_conn(creds: dict):
        conn_str = f"mysql://{creds['user']}:{creds['password']}@{creds['host']}:{creds['port']}/{creds['database']}"
        engine = create_engine(conn_str)
        conn = engine.connect()
        return conn

    def exec_sql(creds, is_autocommit, sql):
        with pymysql.connect(**creds, autocommit=is_autocommit) as conn:
            with conn.cursor() as cur:
                cur.execute(sql)

    def get_iter_cnt(src_val: datetime, dst_val: datetime):
        if not dst_val: # стартовая проливка
            dst_val = get_cutoff(src_creds, 'dt', 'sandbox.transactions', 'min')
        diff = src_val - dst_val
        iter_cnt = int(diff.total_seconds() / 3600) + 1
        return iter_cnt

    def get_cutoff(dst_creds, field, dst_table_name, pos='max'):
        sql = f"select {pos}({field}) from {dst_table_name}"
        with pymysql.connect(**dst_creds, autocommit=True) as conn, conn.cursor() as cur:
            cur.execute(sql)
            cutoff = cur.fetchone()
        if cutoff[0]:
            return cutoff[0] # class 'datetime.datetime'

    def get_src_df(src_conn, cutoff, cnt):
        if not cutoff: # стартовая проливка
            cutoff = get_cutoff(src_creds, 'dt', 'sandbox.transactions', 'min')
        sql = f"""
            select 
                t.*
                ,o.name as name_oper
            from sandbox.transactions t
            left join sandbox.operation_types o
                on t.idoper = o.id
            where t.dt >= '{cutoff}' + interval {cnt} hour and t.dt < '{cutoff}' + interval {cnt + 1} hour
        """
        print(sql)
        df = pd.read_sql(sql, src_conn)
        return df

    src_conn = get_conn(src_creds)
    dst_conn = get_conn(dst_creds)

    # truncate buffer
    exec_sql(dst_creds, True, "truncate table sandbox.transactions_denormalized_buffer")

    cutoff = get_cutoff(dst_creds, 'dt', 'sandbox.transactions_denormalized') # начальная итер смотрит отсечку тарг табл
    src_max_val = get_cutoff(src_creds, 'dt', 'sandbox.transactions')
    cnt = get_iter_cnt(src_max_val, cutoff)

    for iter in range(cnt):
        print('iter =', iter)
        df = get_src_df(src_conn, cutoff, iter)
        if df.empty:
            continue
        print(df)
        df.to_sql(name='transactions_denormalized_buffer', con=dst_conn, if_exists='append', index=False)

    # merge buffer and tgt table
    buff_data = get_cutoff(dst_creds, 'dt', 'sandbox.transactions_denormalized_buffer')
    autocommit = True
    sql = [
        # "START TRANSACTION;",
        "insert into sandbox.transactions_denormalized select * from sandbox.transactions_denormalized_buffer;",
        # "COMMIT;",
    ]
    if cutoff and buff_data:
        sql.insert(0, f"delete from sandbox.transactions_denormalized where dt = '{cutoff}';")
        # autocommit = False
    for query in sql:
        print(query)
        exec_sql(dst_creds, autocommit, query)
